﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(List<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult((IEnumerable<T>)Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(GetById(id));
        }

        public Task CreateAsync(T entity)
        {
            return Task.Run(() => Data.Add(entity));
        }

        public Task UpdateAsync(T entity)
        {
            return Task.Run(() => Data[Data.FindIndex(x => x.Id == entity.Id)] = entity);
        }

        public Task DeleteAsync(Guid id)
        {
            return Task.Run(() => Data.Remove(GetById(id)));
        }

        private T GetById(Guid id)
        {
            return Data.FirstOrDefault(x => x.Id == id);
        }
    }
}